<?php
include('formhandler.class.php');

if(isset($_POST)){
    //print_r($_POST);
}

/* Create a new form handler, bound to the name 'Test'. */
$form = new FormHandler('Test');

/* Create FormElement objects for each input. */
$TestRangeElement = new FormElement('TestRange');
$TestEmailElement = new FormElement('TestEmail');
$TestLengthElement = new FormElement('TestLen');

/* Add constraints to each of our FormElements */
$TestRangeElement->addConstraints(array(FormElement::SIZE_RANGE_CONSTRAINT => '3,10', FormElement::NUMERIC_CHARS_CONSTRAINT));
$TestEmailElement->addConstraints(array(FormElement::SIZE_RANGE_CONSTRAINT => '1,80', FormElement::ALPHA_CHARS_CONSTRAINT));
$TestLengthElement->addConstraints(array(FormElement::MIN_LENGTH_CONSTRAINT => '4', FormElement::MIN_LENGTH_CONSTRAINT => '10'));

/* Add our FormElements to our FormHandler. */
$form->addFormElements(array($TestRangeElement, $TestEmailElement, $TestLengthElement));

/* Process all of our form elements. */
$form->processForm();

/* Check the result of processForm(). */
switch($form->getResultCode()) {
	case FormHandler::UNPROCESSED_RESULT_CODE:
		//This wouldn't occur in this case, but depending on your code, it could.
		break;
	case FormHandler::SUCCESSFUL_RESULT_CODE:
		/* Worked */
		var_dump($form->getResultData());
		break;
	case FormHandler::FAILED_RESULT_CODE:
		/* Failed */
		var_dump($form->getMissingData());
		break;
}


?>