<?php

class FormHandler {

    /** The result code if the FormHandler hasn't been processed. */
    const UNPROCESSED_RESULT_CODE = 0;
    /** The result code if the FormHandler has successfully been processed. */
    const SUCCESSFUL_RESULT_CODE = 1;
    /** The result code if the FormHandler has encountered an error. */
    const FAILED_RESULT_CODE = 2;
    
    /** The name of the form. */
    private $form_name = '';
    
    /** Names of the form elemnts. */
    private $form_element_names = array();
    
    /** The form elements bound to this form. */
    private $form_elements = array();
    
    /** An ID for the result of the processForm() function. */
    private $result_code = 0;
    
    /** If the form has been processed and data was missing, it will be here. */
    private $missing_data = null;
    
    /** Result data */
    private $result_data = array();
    
    /**
     * Creates a new Form Handler class.
     *
     * @param String $form_name The name of the form. 
     */
    public function __construct($form_name) {
        $this->form_name = $form_name;
    }
    
    public function processForm() {
        if(empty($_POST)) {
            return;
        }
		
        /* Verify all form data exists. */
        $missing_data = array();
        foreach($this->form_element_names as $key=>$name) {
			$temp = $this->form_elements[$name]->hasConstraint(FormElement::EMPTY_CONSTRAINT);
            if(!isset($_POST[$name]) || (!$temp && empty($_POST[$name]))) {
                array_push($missing_data, $name);
            }
			unset($temp);
        }
        if(!empty($missing_data)) {
            $this->missing_data = $missing_data;
            $this->result_code = FormHandler::FAILED_RESULT_CODE;
            return;
        }
		
        /* Process all the form elements. */
        foreach($this->form_elements as $element) {
            if(isset($_POST[$element->getName()])) {
                foreach($element->getConstraints() as $key => $constraint) {
                    $post_data = $_POST[$element->getName()];
                    switch($key) {
                        case FormElement::EMAIL_CONSTRAINT:
                            $result = filter_var($post_data, FILTER_VALIDATE_EMAIL);
                            if(is_string($result)) {
                                $result = true;
                            }
                            $element->addConstraintResult(FormElement::EMAIL_CONSTRAINT, $result);
                            break;
                        case FormElement::SIZE_RANGE_CONSTRAINT:
                            $size_range = $element->getConstraint(FormElement::SIZE_RANGE_CONSTRAINT);
                            $element->addConstraintResult(FormElement::SIZE_RANGE_CONSTRAINT, $this->checkSizeRangeConstraint($post_data, $size_range[0], $size_range[1]));
                            break;
                        case FormElement::ALPHA_CHARS_CONSTRAINT:
                            if(strlen($post_data) == 0) {
                                $result = false;
                            } else {
                                $result = ctype_alpha($post_data);
								if(is_string($result)) {
									$result = true;
								}
                            }
                            $element->addConstraintResult(FormElement::ALPHA_CHARS_CONSTRAINT, $result);
                            break;
						case FormElement::NUMERIC_CHARS_CONSTRAINT:
							if(strlen($post_data) == 0) {
                                $result = false;
                            } else {
                                $result = ctype_digit($post_data);
								if(!is_bool($result)) {
									$result = true;
								}
                            }
							$element->addConstraintResult(FormElement::NUMERIC_CHARS_CONSTRAINT, $result);
							break;
						case FormElement::MIN_LENGTH_CONSTRAINT:
							if(strlen($post_data) >= $element->getConstraint(FormElement::MIN_LENGTH_CONSTRAINT)) {
								$result = true;
							} else {
								$result = false;
							}
							$element->addConstraintResult(FormElement::MIN_LENGTH_CONSTRAINT, $result);
							break;
						case FormElement::MAX_LENGTH_CONSTRAINT:
							if(strlen($post_data) <= $element->getConstraint(FormElement::MAX_LENGTH_CONSTRAINT)) {
								$result = true;
							} else {
								$result = false;
							}
							$element->addConstraintResult(FormElement::MAX_LENGTH_CONSTRAINT, $result);
							break;
						case FormElement::EMPTY_CONSTRAINT:
							$is_empty = empty($post_data);
							$should_be_empty = $element->getConstraint(FormElement::EMPTY_CONSTRAINT);
							if(!$should_be_empty && $is_empty) {
								$result = false;
							} elseif(!$should_be_empty && !$is_empty) {
								$result = true;
							}
							$element->addConstraintResult(FormElement::EMPTY_CONSTRAINT, $result);
							break;
                    }
                    $this->result_data[$element->getName()] = $element;
					$this->result_code = self::SUCCESSFUL_RESULT_CODE;
                }
            } else {
                // We shouldn't ever end up here, but just in case.
                throw new Exception("The Form '{$this->form_name}' was missing '{$element->getName()}'");
            }
        }
    }
	
	/**
	 * Adds multiple FormElement's to the form.
	 * 
	 * @param array $elements The elements to be added to the form.
	 */
	public function addFormElements($elements) {
		if($elements instanceof FormElement == true) {
			throw new Exception('It looks like you were trying to use the addFormElement()');
		}
		foreach($elements as $element) {
			$this->addFormElement($element);
		}
	}
	
    /**
     * Adds a FormElement to the form.
     * 
     * @param FormElement $element The element to bind to the form.
     */
    public function addFormElement($element) {
        if($element instanceof FormElement == false) {
			if(is_array($element)) {
				throw new Exception('It looks like you were trying to use the addFormElements()');
			}
            throw new Exception('Tried to add a non form element to a Form.');
        }
		$this->form_elements[$element->getName()] = $element;
        array_push($this->form_element_names, $element->getName());
    }
    
    /**
     * Checks whether $data is within $min_len and $max_len.
     * 
     * @param String $data The data to check.
     * @param int $min_len The minimum length of the data.
     * @param int $max_len The maximum length of the data.
     * @return boolean Whether or not $data matches the size constraints.
     */
    private function checkSizeRangeConstraint($data, $min_len, $max_len) {
        $data_len = strlen($data);
        if($data_len < $min_len || $data_len > $max_len) {
            return false;
        }
        return true;
    }
    
    public function getResultData() {
        return $this->result_data;
    }
    
	public function getResultCode() {
		return $this->result_code;
	}
	
	public function getMissingData() {
		return $this->missing_data;
	}
}

class FormElement {
    
    const EMAIL_CONSTRAINT = 'email';
    const SIZE_RANGE_CONSTRAINT = 'size_range';
    const ALPHA_CHARS_CONSTRAINT = 'alpha_chars';
	const NUMERIC_CHARS_CONSTRAINT = 'numeric_chars';
	const MIN_LENGTH_CONSTRAINT = 'min_length';
	const MAX_LENGTH_CONSTRAINT = 'max_length';
	const EMPTY_CONSTRAINT = 'empty';
    
    /** The name of the FormElement. */
    private $name;
    
    /** An array of the constraints applied to this FormElement. */
    private $constraints = array();
    
    /** The results of the constraint verifications, set be the owner Form. */
    private $constraint_results = array();
    
    public function __construct($name) {
        $this->name = $name;
    }
    
    public function getName() {
        return $this->name;
    }
    
	/**
	 * Adds multiple constraints to the FormElement.
	 * 
	 * @param array $constraints An array of constraints to be added.
	 */
	public function addConstraints($constraints) {
		if(!is_array($constraints)) {
			throw new Exception('It looks like you were trying to use the addConstraint() function.');
		}
		foreach($constraints as $name=>$value) {
			$this->addConstraint($name, $value);
		}
	}
	
    /**
     *
     */
    public function addConstraint($constraint_name, $value = null) {
        switch($constraint_name) {
            case self::EMAIL_CONSTRAINT:
                if(isset($this->constraints[self::EMAIL_CONSTRAINT])) {
                    throw new Exception("Tried to add a second EMAIL_CONSTRAINT constraint to a form element('{$this->name}').");
                }
                $this->constraints[self::EMAIL_CONSTRAINT] = true;
                break;
            case self::SIZE_RANGE_CONSTRAINT:
                if(isset($this->constraints[self::SIZE_RANGE_CONSTRAINT])) {
                    throw new Exception("Tried to add a second SIZE_RANGE constraint to a form element('{$this->name}').");
                }
                if($value != null) {
                    $values = $this->processSizeRangeValue($value);
                    if($values == false) {
                        throw new Exception('The value supplied for the size_range constraint was malformed.');
                    }
                } else {
                    throw new Exception('The size_range constraint requires $value to be set.');
                }
                $this->constraints[self::SIZE_RANGE_CONSTRAINT] = array($values[0], $values[1]);
                break;
            case self::ALPHA_CHARS_CONSTRAINT:
                if(isset($this->constraints[self::ALPHA_CHARS_CONSTRAINT])) {
                    throw new Exception("Tried to add a second apha_chars constraint to a form element('{$this->name}').");
                }
                $this->constraints[self::ALPHA_CHARS_CONSTRAINT] = true;
                break;
			case self::NUMERIC_CHARS_CONSTRAINT:
				if(isset($this->constraints[self::NUMERIC_CHARS_CONSTRAINT])) {
					throw new Exception("Tried to add a second numeric_chars constraint to a form element('{$this->name}').");
				}
				$this->constraints[self::NUMERIC_CHARS_CONSTRAINT] = true;
				break;
			case self::MIN_LENGTH_CONSTRAINT:
				if(isset($this->constraints[self::MIN_LENGTH_CONSTRAINT])) {
					throw new Exception("Tried to add a second min_length constraint to a form element('{$this->name}').");
				}
				if($value == null) {
                    throw new Exception('The min_length constraint requires $value to be set.');
                } elseif(!is_numeric($value)) {
					throw new Exception('The value supplied for the min_length constraint was malformed.');
				}
				$this->constraints[self::MIN_LENGTH_CONSTRAINT] = (int)$value;
				break;
			case self::MAX_LENGTH_CONSTRAINT:
				if(isset($this->constraints[self::MAX_LENGTH_CONSTRAINT])) {
					throw new Exception("Tried to add a second max_length constraint to a form element('{$this->name}').");
				}
				if($value == null) {
                    throw new Exception('The max_length constraint requires $value to be set.');
                } elseif(!is_numeric($value)) {
					throw new Exception('The value supplied for the max_length constraint was malformed.');
				}
				$this->constraints[self::MAX_LENGTH_CONSTRAINT] = (int)$value;
				break;
			case self::EMPTY_CONSTRAINT:
				if(isset($this->constraints[self::EMPTY_CONSTRAINT])) {
					throw new Exception("Tried to add a second empty constraint to a form element('{$this->name}').");
				}
				if($value == null) {
                    throw new Exception('The empty constraint requires $value to be set.');
                }
				$this->constraints[self::EMPTY_CONSTRAINT] = $value;
				break;
            default:
                throw new Exception("Unknown Constraint $constraint_name");
                break;
        }
    }
	
	public function hasConstraint($name) {
		$constraint = @$this->constraints[$name];
		if($constraint == null) {
			return false;
		}
		return true;
	}
    
    public function getConstraint($name) {
        return $this->constraints[$name];
    }
    
    public function getConstraints() {
        return $this->constraints;
    }
    
    private function processSizeRangeValue($value) {
        $result = array();
        $pieces = explode(',', $value);
        
        if(empty($pieces)) {
            return false;
        }
        if(is_numeric($pieces[0])) {
            array_push($result, (int) $pieces[0]);
        } else {
            return false;
        }
        if(is_numeric($pieces[1])) {
            array_push($result, (int) $pieces[1]);
        } else {
            return false;
        }
        return $result;
    }
    
    public function addConstraintResult($constraint_name, $result) {
        $this->constraint_results[$constraint_name] = $result;
    }
    
}

?>