(function($) {
    var methods = {
	init: function(options) {
	    var settings = $.extend({
		'errorField': function() {},
		'validField': function() {}
	    }, options);
	    var valid = true;
	    var formArray = $(this).serializeArray();
	    for (var i = 0; i < formArray.length; i++) {
		var error = false;
		var $formField = $('[name="' + formArray[i].name + '"]');
		$formField.removeClass('error');
		if ($formField.data('empty') !== undefined) {
		    if (($formField.data('empty') === false) && ($formField.val().length === 0)) {
			error = true;
		    }
		}
		if ($formField.data('email') !== undefined) {
		    if (($formField.data('email') !== false)) {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (re.test($formField.val()) === false) {
			    error = true;
			}
		    }
		}
		if ($formField.data('alpha') !== undefined) {
		    if ($formField.data('alpha') !== false) {
			if (/^[a-zA-Z]*$/.test($formField.val()) === false) {
			    error = true;
			}
		    }
		}
		if ($formField.data('numeric') !== undefined) {
		    if ($formField.data('numeric') !== false) {
			if (isNaN(parseFloat($formField.val())) && !isFinite($formField.val())) {
			    error = true;
			}
		    }
		}
		if ($formField.data('regex') !== undefined) {
		    if ($formField.data('regex').test($formField.val()) === false) {
			error = true;
		    }
		}
		if ($formField.data('minlen') !== undefined) {
		    if (parseInt($formField.data('minlen')) > $formField.val().length) {
			error = true;
		    }
		}
		if ($formField.data('maxlen') !== undefined) {
		    if (parseInt($formField.data('maxlen')) < $formField.val().length) {
			error = true;
		    }
		}
		if ($formField.data('rangelen') !== undefined) {
		    var rangeLenParts = $formField.data('rangelen').split(',', 2);
		    var fieldLen = $formField.val().length;
		    if ((fieldLen < parseInt(rangeLenParts[0])) || (fieldLen > parseInt(rangeLenParts[1]))) {
			error = true;
		    }
		}
		if (error === true) {
		    if (settings['errorField'] !== undefined) {
			settings['errorField']($formField);
		    }
		    valid = false;
		} else {
		    if (settings['validField'] !== undefined) {
			settings['validField']($formField);
		    }
		    valid = true;
		}
	    }
	    return valid;
	}
    };
    $.fn.formInputCheck = function(method) {
	if (methods[method]) {
	    return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
	} else if (typeof method === 'object' || !method) {
	    return methods.init.apply(this, arguments);
	} else {
	    $.error('Method ' + method + ' does not exist on jQuery.formInputCheck');
	}
    };
})(jQuery);